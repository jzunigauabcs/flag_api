const express = require('express')
const router = express.Router()

const products = [
    {
        name: 'Green shoes',
        price: 76
    },
    {
        name: 'Black bike',
        price: 123
    }
]

router.get('/', (req, res, next) => {
    res.status(200).json({ products })
})

module.exports = router