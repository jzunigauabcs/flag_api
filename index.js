const express = require('express')
const path = require('path')

const app = express()
const productsApiRouter = require('./routes/api/products')
const productsRouter = require('./routes/views/products')

//Set template pug
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

//Set routes
app.use('/api/products', productsApiRouter)
app.use('/products', productsRouter)
app.get('/', (req, res, next) =>  {
    res.sendFile(path.join(__dirname, 'views/index.html'))
})

app.listen(8000, ()  => {
    console.log(`Connect to port ${8000}`)
})